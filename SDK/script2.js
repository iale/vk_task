let leftList,
    rightList,
    filteredList,
    flexContainer,
    friendsArr,
    template,
    leftFilterInput,
    rightFilterInput,
    leftFriendCollection,
    rightFriendCollection,
    saveButton;

const templateElement = document.querySelector('#user-template'),
    source = templateElement.innerHTML,
    render = Handlebars.compile(source);

document.addEventListener('click', clickEffect);

function api(method, params) {
    return new Promise((resolve, reject) => {
        VK.api(method, params, data => {
            if (data.error) {
                reject(new Error(data.error.error_msg));
            } else {
                resolve(data.response);
            }
        });
    });
}

const promise = () => {
    return new Promise((resolve, reject) => {
        VK.init({
            apiId: 6196280
        });

        VK.Auth.login(data => {
            if (data.session) {
                resolve(data);
            } else {
                reject(new Error('Не удалось авторизоваться'));
            }
        }, 2 | 1024);
    });
} 

promise()
    .then(() => {
        return api('friends.get', { count: 20, v: 5.8, fields: 'id, first_name, last_name, photo_100, city' })
    })
    .then(data => {
        friendsArr = data.items;
        const lists = {
            list: [],
            list2: [],            
        };

        if (localStorage.savedFriends) {
            const savedFriends = JSON.parse(localStorage.savedFriends);

            savedFriends.forEach((savedFriendID) => {

                data.items.forEach((friend) => {
                    if (friend.id == savedFriendID) {
                        lists.list2.push(friend);
                    }
                })
            })

            lists.list = data.items.filter(
                function (e) { return this.indexOf(e) < 0; }, lists.list2
            );


        } else {
            lists.list = data.items
            lists.list2 = []; 
        }

        template = render(lists);
        results.innerHTML = template;
        
        data = { 
            friends: data.items,
        }

        return data

    })
    .then(() => {
        dragActions();
    })
    .then(() => {
        getNodes();
    })
    .catch(function (e) {
        alert('Ошибка: ' + e.message);
    });

const getNodes = () => {
    leftFilterInput = document.querySelector('.left-input-field');
    rightFilterInput = document.querySelector('.right-input-field');
    leftFriendCollection = document.querySelector('.left-list').children;
    rightFriendCollection = document.querySelector('.right-list').children;
    saveButton = document.querySelector('.save-button');

    leftFilterInput.addEventListener('keyup', filterFriends);
    rightFilterInput.addEventListener('keyup', filterFriends);    
    saveButton.addEventListener('click', saveFilters);
}

function clickEffect(e) {
    const d = document.createElement('div');

    d.className = 'clickEffect';
    d.style.top = e.clientY + 'px'; d.style.left = e.clientX + 'px';
    document.body.appendChild(d);
    d.addEventListener('animationend', function () { d.parentElement.removeChild(d); }.bind(this));
}

const saveFilters = () => {

    const filteredFriends = [];

    for (let friendNode of rightFriendCollection) {
        filteredFriends.push(friendNode.id)
    }

    localStorage.savedFriends = JSON.stringify(filteredFriends);

    alert('Списки сохранены');
}

function contains(list, value) {
    for (var i = 0; i < list.length; ++i) {
        if (list[i] === value) return true;
    }

    return false;
}

function dragActions() {
    leftList = document.querySelector('.left-list')    
    rightList = document.querySelector('.right-list');
    filteredList = document.querySelector('.filtered-list');
    flexContainer = document.querySelector('.flex-container');
    
    leftList.addEventListener('dragstart', (e) => {
        if (e.target.classList.contains('friend-row')) {
            const friendHTML = e.target.outerHTML;

            e.dataTransfer.effectAllowed = 'move';
            e.dataTransfer.setData('text/html', friendHTML);
        }
    })

    filteredList.addEventListener('dragover', (e) => {
        // if (e.target.classList.contains('filtered-list')) {
        var isHTML = contains(e.dataTransfer.types, 'text/html');

        if (isHTML) {
            e.preventDefault();
        }
        // }
    })

    // filteredList.addEventListener('dragenter', (e) => {
    //     console.log('enter e', e.target);
    //     if (e.target.classlist) {
    //         console.log('222');
            
    //     }
    //     if (e.target.classlist.contains('filtered-list')) {
    //         filteredList.classlist.add('drop-zone');
    //         console.log('888');
            
    //     }
        
    // })

    // filteredList.addEventListener('dragleave', (e) => {
    //     console.log('leave e', e.target);
    //     if (e.target.classlist) {
    //         console.log('333');
            
    //     }
        
    //     if (e.target.classlist.contains('filtered-list')) {
    //         filteredList.classlist.remove('drop-zone');
    //         console.log('999');            
    //     }
    // })

    filteredList.addEventListener('drop', (e) => {
        e.preventDefault();
        var data = e.dataTransfer.getData('text/html');

        var div = document.createElement('div');

        div.innerHTML = data;
        var transferredElement = div.firstChild;

        const actionIcon = transferredElement.querySelector('.filter-icon');

        actionIcon.classList.remove('fa-plus', 'filter-icon');
        actionIcon.classList.add('fa-times', 'unfilter-icon');

        if (transferredElement.classList.contains('friend-row')) {
            rightList.appendChild(transferredElement)
            leftList.removeChild(document.getElementById(transferredElement.id))
        }

    })

    flexContainer.addEventListener('click', (e) => {
        if (e.target.classList.contains('unfilter-icon')) {
            sendFriendBack(e)
        }

        if (e.target.classList.contains('filter-icon')) {
            const friendRow = e.target.parentNode;

            changeIcon(null, friendRow);          
            rightList.appendChild(friendRow)
            // rightList.removeChild(friendRow)

            // leftList.removeChild(document.getElementById(friendRow.id))
            // leftList.removeChild(friendRow)

        }
    })
}

function changeIcon(event, friend=null) {
    let friendRow;

    if (event) {
        friendRow = event.target.parentNode;
    } else if (friend) {
        friendRow = friend
    } else console.log('error changeIcon');
    
    let actionIcon = friendRow.querySelector('.fa');

    if (actionIcon.classList.contains('unfilter-icon')) {

        actionIcon.classList.add('fa-plus', 'filter-icon');
        actionIcon.classList.remove('fa-times', 'unfilter-icon');

    } else if (actionIcon.classList.contains('filter-icon')) {

        actionIcon.classList.remove('fa-plus', 'filter-icon');
        actionIcon.classList.add('fa-times', 'unfilter-icon');

    }

    return friendRow;
}

function sendFriendBack(event) {

    const friendRow = changeIcon(event);
    
    leftList.appendChild(friendRow)
    rightList.removeChild(friendRow)
    
}

function isMatching(full, chunk) {
    full = full.toLowerCase();
    chunk = chunk.toLowerCase();

    return full.includes(chunk);
}

const filterFriends = (e) => {

    let filteringFriendsList,
        input,
        friendNode;
    
    if (e.target.classList.contains('left-input-field')) {
        input = leftFilterInput.value;
        filteringFriendsList = leftFriendCollection;
    }

    if (e.target.classList.contains('right-input-field')) {
        input = rightFilterInput.value;
        filteringFriendsList = rightFriendCollection;
    }

    if (!input) {
        for (friendNode of filteringFriendsList) {
            friendNode.classList.remove('hidden')
        }

        return;
    }

    let filteredFriends = friendsArr.filter((friend) => {
        return (
            isMatching(friend.first_name, input) ||
            isMatching(friend.last_name, input)
        )
    })

    for (friendNode of filteringFriendsList) {
        friendNode.classList.add('hidden');
        filteredFriends.forEach((friendElem) => {
            if (friendElem.id == friendNode.id) {                
                friendNode.classList.remove('hidden')
            }

        })
    }
}